# Alteração de configuração de DNS

Substituir a entrada de DNS tipo A por uma CNAME.

Para fazer esta alteração remova a entrada de tipo A que aponta para o servidor da Playfacil (167.99.228.225). E adicione uma nova entrada CNAME com o mesmo nome de host apontando para o endereço app.playfacil.com.br

Segue exemplo abaixo da remoção do entrada tipo A e criação da entrada tipo CNAME.

![](docs/cname.png)

Após salvar as alterações você pode testar a configuração utilizando o comando nslookup no CMD ou Bash. Substitua o endereço playfacil.jhtoigo.cloud pelo seu endereço de domínio configurado.

A resposta deve apontar para app.playfacil.com.br, que por sua vez vai resolver o endereço IP do servidor da Playfacil.

![](docs/test.png)

